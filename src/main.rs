fn print_header(title: &str) {
    println!("\n");
    println!("{}", title);

    for _char in 0..title.len() { print!("=") }
    print!("\n");
}

fn main() {
    print_header("Basic Math:");
    let result = 5 + 3;
    println!("5 + 3 = {}", result);

    let mut mutable_result = 60;
    mutable_result += 9;
    println!("yes: {}", mutable_result);



    print_header("Strings:");
    let mut the_answer = String::from("The ");
    the_answer.push_str("ultimate ");
    println!("{}", the_answer);

    the_answer.push_str("answer ");
    println!("{}", the_answer);

    the_answer.push_str("to live, ");
    println!("{}", the_answer);

    the_answer.push_str("the universe, ");
    println!("{}", the_answer);

    the_answer.push_str("and everything ");
    println!("{}", the_answer);

    the_answer.push_str("is ");
    println!("{}", the_answer);

    the_answer.push_str("42 ");
    println!("{}\n", the_answer);

    println!("THE ANSWER takes up {} out of {} characters", the_answer.len(), the_answer.capacity());
    println!("Is it empty? {}", the_answer.is_empty());
    println!("Does it contain '42'? {}\n", the_answer.contains("42"));

    for word in the_answer.split_whitespace() {
        println!("{}", word)
    }
}
